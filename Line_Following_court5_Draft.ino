
#include <Servo.h>  // Include Servo Library

// Declare QTI pins
#define RIGHT_QTI_PIN 52
#define CENTER_QTI_PIN 53
#define LEFT_QTI_PIN 51

// Declare white/black min/max
#define WHITE_MAX 40

#define LOOP_SPEED 50 // Hz; number of times loop runs per second

int loopDelay = 1000 / LOOP_SPEED; // Calculate the loop delay from the loop speed

enum STATES { NONE, CHARACTER_SENT, CHARACTER_RECEIVED, PAUSING_DRIVE }; // Define an emueration of possible states
int NEXT_STATE_AFTER[] {NONE, NONE, NONE, NONE }; // What to do after each state terminates
int STATE_TIMEOUTS[] { -1, 1000, 1000, 1000 }; // How long we should wait before exiting state

enum QTI_PINS { LEFT, CENTER, RIGHT }; 
enum DIRECTIONS { DIR_LEFT, DIR_STRAIGHT, DIR_RIGHT, DIR_PAUSE, DIR_STOP, DIR_BW };

Servo servoLeft;    // Declare Left and Right Servos
Servo servoRight;

void setup() {

    Serial.begin(9600);  // Communicate with the host computer at 9600 baud
    Serial2.begin(9600); // Communicate with the XBee antenna at 9600 baud
    
    servoLeft.attach(12);
    servoRight.attach(11);
    
    pinMode(RIGHT_QTI_PIN, INPUT);
    pinMode(CENTER_QTI_PIN, INPUT);
    pinMode(LEFT_QTI_PIN, INPUT);

    delay(500); // Wait 500 ms before starting to check for signals (this might be removed later)
}

int stateClockMillis = 0; // Keep track of how long we have been in a given state
long lastMillis = 0;
int currentState = NONE;  // The initial state is "not doing anything"
long sensorReadings[] = { 0, 0, 0 }; 

void loop() {
    if (currentState != NONE) { // If we are actively doing something
        if (stateClockMillis >= STATE_TIMEOUTS[currentState]) { // Check if time has expired
            setState(NEXT_STATE_AFTER[currentState]); // Change state to whatever would be next
        } else {
            long currentTime = millis(); // Retrieve the current time
            stateClockMillis += (currentTime - lastMillis); // Add the elapsed time since the last loop
            lastMillis = currentTime; // Set the last time to the current time
        }
    }

    int intendedDirection = getIntendedDirection();

    switch (intendedDirection) {
        case DIR_LEFT:
            turnLeft();
            break;
        case DIR_RIGHT:
            turnRight();
            break;
        case DIR_STRAIGHT: 
            if (currentState != PAUSING_DRIVE) goStraight();
            break;
        case DIR_PAUSE:
            
            stopMotors();
            delay(500);
            goStraight();
            delay(200);
            break;
        case DIR_BW:
            goBackwards();
            break;
        case DIR_STOP:
                  
       default:
       stopMotors();
            
    }

    delay(loopDelay); // Wait for the previously calculated amount of time
}


long RCtime(int sensPin){

   long result = 0;

   pinMode(sensPin, OUTPUT);       // make pin OUTPUT

   digitalWrite(sensPin, HIGH);    // make pin HIGH to discharge capacitor - study the schematic

   delay(1);                       // wait a  ms to make sure cap is discharged

   pinMode(sensPin, INPUT);        // turn pin into an input and time till pin goes low

   digitalWrite(sensPin, LOW);     // turn pullups off - or it won't work

   while(digitalRead(sensPin)){    // wait for pin to go low

      result++;

   }

   return result;                   // report results
}

int getIntendedDirection() {
    getSensorReadings();

    bool leftIsBlack = (sensorReadings[LEFT] > 60);
    bool centerIsBlack = (sensorReadings[CENTER] > 150);
    bool rightIsBlack = (sensorReadings[RIGHT] > 50);
    bool leftIsWhite= (sensorReadings[LEFT] < 25);
   bool centerIsWhite= (sensorReadings[CENTER] < 50);
    bool rightIsWhite=(sensorReadings[RIGHT] < 33);
    
    
    Serial.print(sensorReadings[LEFT]);
    Serial.print(","); 
    Serial.print(sensorReadings[CENTER]);
    Serial.print(","); 
    Serial.println(sensorReadings[RIGHT]);

    Serial.print(leftIsBlack);
    Serial.print(centerIsBlack);
    Serial.println(rightIsBlack);

   // if (!centerIsBlack) return DIR_STOP;

    if (leftIsBlack && rightIsBlack && centerIsBlack) return DIR_PAUSE;
    if (leftIsWhite && rightIsWhite && centerIsWhite) return DIR_BW;

    if (rightIsBlack) return DIR_RIGHT;
    if (leftIsBlack) return DIR_LEFT;
    
    return DIR_STRAIGHT;
}

void turnRight() {
    servoLeft.writeMicroseconds(1550);         // Left wheel counterclockwise
    servoRight.writeMicroseconds(1550);        // Right wheel counterclockwise
}

void turnLeft() {
    servoLeft.writeMicroseconds(1450);         // Left wheel clockwise
    servoRight.writeMicroseconds(1450);        // Right wheel clockwise
}

void goStraight() {
    servoLeft.writeMicroseconds(1550);         // Left wheel counterclockwise
    servoRight.writeMicroseconds(1450);        // Right wheel clockwise
}

void stopMotors() {
    servoLeft.writeMicroseconds(1500);         // Left wheel stop
    servoRight.writeMicroseconds(1500);        // Right wheel stop
}
void goBackwards(){
     servoLeft.writeMicroseconds(1450);         // Left wheel clockwise
    servoRight.writeMicroseconds(1550);        // Right wheel counterclockwise
}

void getSensorReadings() {
    sensorReadings[LEFT] = RCtime(LEFT_QTI_PIN);
    sensorReadings[CENTER] = RCtime(CENTER_QTI_PIN);
    sensorReadings[RIGHT] = RCtime(RIGHT_QTI_PIN);
}

void setState(int state) {
    currentState = state; // Set the current state to `state`
    stateClockMillis = 0; // Reset the state clock
}
